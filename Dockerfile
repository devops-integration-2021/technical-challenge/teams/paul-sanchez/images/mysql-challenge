FROM mysql:5.6

MAINTAINER Paul Sanchez <sanchez.u.1992@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
